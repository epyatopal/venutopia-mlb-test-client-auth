var app = angular.module('directives', [])
    .directive('nicescroll', function() {
        function link(scope, element, attrs) {
            element.niceScroll()
        }
        return {
            link: link
        };
    })
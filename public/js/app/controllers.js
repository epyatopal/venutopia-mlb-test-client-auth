'use strict';

/* Controllers */

var controllers = angular.module('controllers', [])

controllers.controller('CommonCtrl', ['$scope', '$rootScope', '$routeParams', '$location', 'API',
    function ($scope, $rootScope, $routeParams, $location, API) {
        $rootScope.userCredentials = {
            google: false,
            facebook: false
        }

        $rootScope.checkAuth = function(){
            return ($rootScope.user) ? true : false;
        }

        $rootScope.checkSocialAuth = function(){
            var result = false;
            if ($rootScope.userCredentials.google) result = $rootScope.userCredentials.google
            else if ($rootScope.userCredentials.facebook) result = $rootScope.userCredentials.facebook;
            return result;
        }

        $scope.username="";
        $scope.init = function(){
        };
        $scope.init();

        $scope.enter = function($event){

            if($event.which.toString() == '13'){
                $('#nickname').blur()
                $event.preventDefault()
                $scope.addName()
            }
        }

        $scope.addName = function(){
            if(!$scope.username.length) return $scope.errorname = "Enter name please"
            API.request('/user/' +  $rootScope.user._id, null, {username: $scope.username}, "PUT").success(function (data) {
                if(data.err) return $scope.errorname = data.err
                $rootScope.user.username = $scope.username
//                $rootScope.notName = false
            })

        }

        $scope.updateProfile = function(){
            $scope.errorname = false
                API.request('/user/' +  $rootScope.user._id, null, {username: $rootScope.user.username, email: $rootScope.user.email}, "PUT").success(function (data) {
                    if(data.err) return $scope.errorname = data.err
                    $location.path('/gamepin')
                })
        }

        $scope.logout = function(){
            API.request('/auth/logout').success(function (data, status, headers, config) {
                window.location.href = '/';
            }).error(function (data, status, headers, config) {
                });
        }

//        $rootScope.userLoad = function() {
//            API.request('/user/').success(function (data, status, headers, config) {
//                if (data == 'error') return
////                console.log('data ====>>>>> ', data)
//                $rootScope.user = data
//                $rootScope.user.scoreShow = Math.round($rootScope.user.score)
//            }).error(function (data, status, headers, config) {
//                });
//        }
    }]);

controllers.controller('AuthCtrl', ['$scope', '$rootScope', '$routeParams', '$location', '$timeout', '$interval', 'API', '$cookieStore', 'userAuthService', '$http',
    function ($scope, $rootScope, $routeParams, $location, $timeout, $interval, API, $cookieStore, userAuthService, $http) {

        var clientId = '342361308357-q57ke3r7ngq3k6o659ticn59g7ivpp6b.apps.googleusercontent.com'
            , apiKey = 'fgpwIHiMKCLxMaY9uKMl9h0r'

        // To enter one or more authentication scopes, refer to the documentation for the API.
            , scopes = 'https://www.googleapis.com/auth/plus.me';
//            , scopes = 'profile';

        function logResponse(resp) {
            console.log(resp);
        }

        // Use a button to handle authentication the first time.
        $scope.onAuthGoogle = function(){

            if ($rootScope.checkSocialAuth()) return $rootScope.checkSocialAuth();
            gapi.client.setApiKey(apiKey);
            var config = {
                'client_id': clientId,
                'scope': scopes
            };

            gapi.auth.authorize(config, function() {
                var token = gapi.auth.getToken()
                $rootScope.userCredentials.google = {
                    token: gapi.auth.getToken()
                    , type: 'google'
                }
                $location.path('/gamepin')
                userAuthService.load();
                $http.get('https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=' + token.access_token)
                .success(function(obj){
                        console.log('obj', obj)
//                        $rootScope.userCredentials.google = {
//                            profile:obj
//                            , token: gapi.auth.getToken()
//                            , type: 'google'
//                        }
//                        $location.path('/gamepin')
//                        userAuthService.load();
                    })
                .error(function(error){
                        console.log('error', error)
                    })


            })
        }

        $scope.onAuthFacebook = function(){
//            FB.login(function(response) {
//                if (response.authResponse) {
//                    console.log('Welcome!  Fetching your information.... ');
//                    FB.api('/me', function(response) {
//                        console.log('Good to see you, ' + response.name + '.');
//                    });
//                } else {
//                    console.log('User cancelled login or did not fully authorize.');
//                }
//            });

            if ($rootScope.checkSocialAuth()) return $rootScope.checkSocialAuth();
            FB.login(function(response) {
                console.log('response', response)
                $rootScope.userCredentials.facebook = {
                    token: response.authResponse
                    , type: 'facebook'
                }
                $location.path('/gamepin')
                userAuthService.load();
//                $rootScope.userCredentials.facebook = {
//                    token: response.authResponse
//                    , type: 'facebook'
//                }
//                FB.api('/' +response.authResponse.userID, function(profileResp) {
//                    $rootScope.userCredentials.facebook = {
//                        profile: profileResp
//                        , token: response.authResponse
//                        , type: 'facebook'
//                    }
//                    console.log(profileResp);
//                    $location.path('/gamepin')
//                    userAuthService.load();
//                });


            }, {scope: 'public_profile, email',
                return_scopes: true});
        }
//        $scope.checkAuth = function(){
//            var config = {
//                'client_id': clientId,
//                'scope': scopes
//            };
//            gapi.auth.authorize(config, function() {
////                $location.path('/gamePin')
//                console.log('login complete');
//                console.log(gapi.auth.getToken());
//                console.log('=====================>>>> ', arguments);
//                $rootScope.userCredentials.google = gapi.auth.getToken()
//
////                $cookieStore.put('user', $scope.google)
////                $cookieStore.put('social', 'google')
////                var changeLocation = function(url, forceReload) {
////                    $scope = $scope || angular.element(document).scope();
////                    if(forceReload || $scope.$$phase) {
////                        window.location = url;
////                    }
////                    else {
////                        //only use this if you want to replace the history stack
////                        //$location.path(url).replace();
////
////                        //this this if you want to change the URL and add it to the history stack
////                        $location.path(url);
////                        $scope.$apply();
////                    }
////                };
////                changeLocation('/gamepin')
//            });
//
//        }


    }]);

controllers.controller('PlayCtrl', ['$scope', '$rootScope', '$routeParams', '$location', '$timeout', '$interval', 'API',
    function ($scope, $rootScope, $routeParams, $location, $timeout, $interval, API) {
        if($routeParams.id) {
            API.request('/campaign/' +  $routeParams.id).success(function (data, status, headers, config) {
                if (data == 'error') return
                $rootScope.campaignMessage = data
                console.log('data', data)
                if (data == 'error') $location.path('/play')
            }).error(function (data, status, headers, config) {
            });
        }else{
            API.request('/campaign').success(function (data, status, headers, config) {
                if (data == 'error') return $rootScope.campaignMessage = ''
                $rootScope.campaignMessage = data[Math.floor(Math.random()*data.length)]
            }).error(function (data, status, headers, config) {
                });}

        $scope.sec = 0
        $scope.milisec = 0
        $scope.min = ''

        $scope.attachTimer = function($event) {

            if($event.which.toString() == '13'){
                $event.preventDefault()
                $scope.submitScore()
            }
            if ($scope.sec == 0 && $scope.milisec==0 && $event.which.toString() != '13') {
                $scope.timeStart = new Date();
                $scope.gameStart = true
                $scope.stop = $interval(function() {
                    var timeEnd = new Date()
                        , timeDisp = new Date();
                    $scope.timeDifference = timeEnd.getTime() - $scope.timeStart.getTime();
                    timeDisp.setTime($scope.timeDifference)
                    var minutes_passed = timeDisp.getMinutes();
                    if (minutes_passed > 0) {
                        $scope.min = timeDisp.getMinutes();
                        $scope.min = ($scope.min < 10) ? "0" + $scope.min + ':' : $scope.min + ':'
                    }
                    $scope.sec = timeDisp.getSeconds();
                    if($scope.sec < 10){
                        $scope.sec = "0" + $scope.sec;
                    }
                    $scope.milisec = timeDisp.getMilliseconds();
                    if($scope.milisec < 10){
                        $scope.milisec = "00" + $scope.milisec;
                    }
                    else if($scope.milisec < 100){
                        $scope.milisec = "0" + $scope.milisec;
                    }

                }, 100);
            }


        }

        $scope.gameSave = function(data) {
            API.request('/game/', null, data, 'post').success(function (data, status, headers, config) {
                if (data == 'error') return
                $location.path('/result/' + data._id)
                API.request('/game/', null, data, 'put').success(function (data, status, headers, config) {
//                    if (data == 'error') return
//                $rootScope.score = Math.round(final_score)
//                $rootScope.result = result

                }).error(function (data, status, headers, config) {
                    });
//                $rootScope.score = Math.round(final_score)
//                $rootScope.result = result
                // $location.path('/play/' + data._id)
            }).error(function (data, status, headers, config) {
                });

        }



        $scope.submitScore = function() {

            if (angular.isDefined($scope.stop)) {
                $interval.cancel($scope.stop);
                $scope.stop = undefined;
            }

            var resultArr = $rootScope.campaignMessage.text.split('')
                , resultStr = $rootScope.campaignMessage.text
                , userArr = $scope.submissionText.split('')
                , result = []
                , errorSymbol = []
                , gl_pos = 0;
//            if($scope.sponsorMessage.indexOf($scope.submissionText) + 1) {
//                $rootScope.result = result
//                return $location.path('/result')
//            }
            for (var i= 0, l=userArr.length; i<l; i++) {
                var pos = resultStr.indexOf(userArr[i])
                if (pos != -1 && pos >= gl_pos) {
                    result.push(pos)
                    gl_pos = pos
                    var tempStr = ''
                    for (var t = 0; t <= pos; t++) {
                        tempStr = tempStr + '~'
                    }
                    resultStr = tempStr + resultStr.substring(pos+1);
                } else if (pos != -1 && pos < gl_pos) {
                    resultStr = resultStr.substring(0, pos) + '~' + resultStr.substring(pos+1);
                    i--
                    gl_pos = pos
                } else {
                    var er = {
                        pos: (resultStr[0]=='~') ? gl_pos+1 : gl_pos
                        , letter: userArr[i]
                    }
                    errorSymbol.push(er)
                }
            }
            $rootScope.time = parseInt($scope.timeDifference) / 1000.00
            var time = parseFloat($rootScope.time)
                , temp_raw = 100.00 * (($scope.submissionText.length/time)/6.168)
                , raw_score = Math.min(99, temp_raw)
                , accuracy = result.length / resultArr.length * 100
                , inaccuracy = 100.00 - accuracy
                , final_score = raw_score - inaccuracy
                , final_score = Math.max(0, final_score)
                , final_score = Math.min(final_score, 100)
            console.log('final_score====>>>', final_score)
            var dataGameSave = {
                user_id: $rootScope.user._id
                , campaign_id: $rootScope.campaignMessage._id
                , score: final_score
                , time: time
                , result: result
                , errorSymbol: errorSymbol
            }
//            if ($rootScope.user.scope && $rootScope.user.scope>=final_score) return $scope.gameSave(dataGameSave)
            var params = {
                error: resultArr.length - result.length
                , time: time
                , score: final_score
            }

//            console.log('params!!!!!!!!!!!!!!!!!!!!!!!!!', params)
            API.request('/user', params, params, 'PUT').success(function (data, status, headers, config) {
                if (data == 'error') return
//                $rootScope.userLoad()

                $scope.gameSave(dataGameSave)
//                $rootScope.score = Math.round(final_score)
//                $rootScope.result = result
//                $location.path('/play/123')
            }).error(function (data, status, headers, config) {
                });

//            $rootScope.score = Math.round(final_score)
//            $rootScope.result = result
//            return $location.path('/result')
        }

    }]);

controllers.controller('ResultCtrl', ['$scope', '$rootScope', '$routeParams', '$location', 'API',
    function ($scope, $rootScope, $routeParams, $location, API) {
        $scope.params=$routeParams.id
        API.request('/game/' +  $scope.params).success(function (data, status, headers, config) {
            if (data == 'error') return
            $scope.game = data.game
            $rootScope.count = data.count
            $scope.game.score = Math.round($scope.game.score)

            $scope.load()
//            console.log('----->>>>>>>', data)

            //your place in the overall standings
            API.request('/game/position/'+$scope.game.campaign._id).success(function (data, status, headers, config) {
                if (data == 'error') return

                var groupGames = _.toArray(_.groupBy(data.games, function(game){
                    return game.user
                }))
                var users = []
//
                groupGames.forEach(function(games){
                    var user = games.sort(function (a, b) {
                        if (a.score < b.score) return 1;
                        if (b.score < a.score) return -1;
                        if (a.time < b.time) return -1;
                        if (b.time < a.time) return 1;
                        return 0;
                    })
                    users.push(user[0])
                })

                var sortGames = users.sort(function (a, b) {
                    if (a.score < b.score) return 1;
                    if (b.score < a.score) return -1;
                    if (a.time < b.time) return -1;
                    if (b.time < a.time) return 1;
                    return 0;
                })
                $scope.myPosition = ordinalNumber(_.indexOf(sortGames, _.findWhere(sortGames, {user: $scope.game.user._id}))+1);
//                console.log('--position--->>>>>>>', groupGames, users, sortGames, $scope.myPosition)
            }).error(function (data, status, headers, config) {
                });

        }).error(function (data, status, headers, config) {
            });

        $scope.load = function() {
            $scope.resultArr = []
            $scope.game.campaign.text.split('').forEach(function(el, count){
                var t = {
                    name: el
                    , correct: false
                }
                $scope.resultArr.push(t)
            })
            $scope.game.result.forEach(function(el, count){
                $scope.resultArr[el].correct = true
            })
            $scope.game.errorSymbol.forEach(function(el, count){
//                $scope.resultArr[el.pos].error = true
//                $scope.resultArr[el.pos].name = el.letter
//                if (el.pos < $scope.game.campaign.text.length) {
                    var t = {
                        name: el.letter
                        , error: true
                    }
                    $scope.resultArr.splice(el.pos+count, 0 , t)
//                }

            })
        }


    }]);

controllers.controller('LeaderboardCtrl', ['$scope', '$rootScope', '$routeParams', '$location', 'API',
    function ($scope, $rootScope, $routeParams, $location, API) {

//        API.request('/user/all').success(function (data, status, headers, config) {
//            if (data == 'error') return
//            $scope.results = data
//            $scope.results.forEach(function(el){
//                el.score = Math.round(el.score)
//            })
//        }).error(function (data, status, headers, config) {
//            });

        API.request('/game/campaign/'+$routeParams.id).success(function (data, status, headers, config) {
            var sortGames = _.toArray(_.groupBy(data.games, function(game){
                return game.user._id
            }))
            var users = []
            data.games.length && ($scope.campaign = data.games[0].campaign)

            sortGames.forEach(function(games){
                var user = {}
                user._id = games[0].user._id
                user.name = games[0].user.username
//                var bestGame = _.max(games, function(game){
//                    return game.score
//                })

                var bestGame = games.sort(function (a, b) {
                    if (Math.round(a.score) < Math.round(b.score)) return 1;
                    if (Math.round(b.score) < Math.round(a.score)) return -1;
                    if (a.time < b.time) return -1;
                    if (b.time < a.time) return 1;
                    return 0;
                })[0]
                user.score = bestGame.score
                user.time = bestGame.time
                user.error = bestGame.errorSymbol.length
                user.submissions = games.length
                users.push(user)
            })
//            users = users.sort(function(user1, user2){
//                return user2.score - user1.score
//            })
//
//            console.log('users', users)
//
//            $scope.users = users
            $scope.users = users.sort(function (a, b) {
                if (Math.round(a.score) < Math.round(b.score)) return 1;
                if (Math.round(b.score) < Math.round(a.score)) return -1;
                if (a.time < b.time) return -1;
                if (b.time < a.time) return 1;
                return 0;
            })
//            console.log('----->>>>>>>', $scope.users)

            $scope.myResult = _.find($scope.users, function(user, index){
                user.rank = ordinalNumber(index+1)
                console.log('index', user.name, $rootScope.user.username)
                return (user._id == $rootScope.user._id)
            })

        })

    }]);

controllers.controller('GamePinCtrl', ['$scope', '$rootScope', '$routeParams', '$location', 'API',
    function ($scope, $rootScope, $routeParams, $location, API) {
        $scope.checkPin = function(){
            API.request('/campaign/' +  $scope.pin).success(function (data) {
                if(data != 'err'){
                    $location.path('/play/'+$scope.pin)
                }else{
                    $scope.error = true
                }

            })
        }

        $scope.enter = function($event){
//            $event.stopPropagation()

            $scope.error = false
            if($event.which.toString() == '13'){
                $('#pin').blur()
                $event.preventDefault()
                $scope.checkPin()
            }
        }

    }]);

controllers.controller('RatingCtrl', ['$scope', '$rootScope', '$routeParams', '$location', 'API',
    function ($scope, $rootScope, $routeParams, $location, API) {
        $scope.formData = {};

        $scope.saveRating = function(data) {
            API.request('/rating/', null, data, 'post')
                .success(function (data, status, headers, config) {
                    if (data == 'error') return;

                    // goto gamepin after save
                    $location.path('/gamepin' + data._id);


                    API.request('/rating/', null, data, 'put')
                        .success(function (data, status, headers, config) {
                            if (data == 'error') return;
                        })
                        .error(function (data, status, headers, config) {
                            console.log('rating save error occurred');
                        });
                })
                .error(function (data, status, headers, config) {
                    console.log('rating save error occurred');
                });
        };

        // when SUBMITTING the add form, send the text to the node API ... use service
        $scope.submitRating = function() {
            $scope.errors = [];
            $scope.msgs = [];

            // validate that is not empty
            if(!$.isEmptyObject($scope.formData)) {
                // check for required fields
                if (!$scope.formData.value) $scope.errors.push('Rating is a required field.');
                if($scope.errors.length > 0) return $scope.errors;

                // create rating record
                var dataRating = {
                    user_id: $rootScope.user._id,
                    name: $rootScope.user.username,
                    email: $rootScope.user.email,
                    value: $scope.formData.value,
                    comments: $scope.formData.comments
                };
                $scope.saveRating(dataRating);
                $scope.formData = {}; // clear form so our user is ready to enter another
            };
        };
    }]);

var ordinalNumber = function(number){
    console.log('number', number)
    var str = number.toString()
    if(str.length>2  && (str.slice(-2) == '11' || str.slice(-2) == '12' || str.slice(-2) == '13')){
        return str+'th'
    }else{
        var strlast = str.slice(-1)
            , ord
        switch(strlast) {
            case '1':
                ord = 'st';
                break;
            case '2':
                ord = 'nd';
                break;
            case '3':
                ord = 'rd';
                break;
            default:
                ord ='th'
                break;
        }
        return str+ord
    }
};



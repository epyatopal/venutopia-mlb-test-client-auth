'use strict';

//window.remoteHost = 'http://localhost:9000'
// Declare app level module which depends on filters, and services
var app = angular.module('app', [
            'ngRoute',
            'ngCookies',
            'controllers',
            'services',
            'directives']);

app.config(['$routeProvider', '$locationProvider', '$httpProvider', function ($routeProvider, $locationProvider, $httpProvider) {
    //Enable cross domain calls
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common["X-Requested-With"]
    $routeProvider
        .when('/play/', {
            templateUrl: '/partials/play.html',
            controller: 'PlayCtrl',
            resolve: {
                load: function(userAuthService){
                    return userAuthService.load()
                }
            }
        })
        .when('/auth/', {
            templateUrl: '/partials/auth.html',
            controller: 'AuthCtrl',
            resolve: {
                load: function(userAuthService){
                    return userAuthService.authLoad()
                }
            }

        })
        .when('/play/:id',{
            templateUrl: '/partials/play.html',
            controller: 'PlayCtrl',
            resolve: {
                load: function(userAuthService){
                    return userAuthService.load()
                }
            }
        })
        .when('/gamepin', {
            templateUrl: '/partials/gamepin.html',
            controller: 'GamePinCtrl',
            resolve: {
                load: function(userAuthService){
                    return userAuthService.load()
                }
            }
        })
        .when('/result/:id', {
            templateUrl: '/partials/result.html',
            controller: 'ResultCtrl',
            resolve: {
                load: function(userAuthService){
                    return userAuthService.load()
                }
            }
        })
        .when('/leaderboard/:id', {
            templateUrl: '/partials/leaderboard.html',
            controller: 'LeaderboardCtrl',
            resolve: {
                load: function(userAuthService){
                    return userAuthService.load()
                }
            }
        })
        .when('/contactus', {
            templateUrl: '/partials/contactus.html',
            resolve: {
                load: function(userAuthService){
                    return userAuthService.load()
                }
            }
        })
        .when('/funfacts', {
            templateUrl: '/partials/funfacts.html',
            resolve: {
                load: function(userAuthService){
                    return userAuthService.load()
                }
            }
        })
        .when('/privacy', {
            templateUrl: '/partials/privacy.html',
            resolve: {
                load: function(userAuthService){
                    return userAuthService.load()
                }
            }
        })
        .when('/terms', {
            templateUrl: '/partials/terms.html',
            resolve: {
                load: function(userAuthService){
                    return userAuthService.load()
                }
            }
        })
        .when('/rate', {
            templateUrl: '/partials/rate.html',
            controller: 'RatingCtrl',
            resolve: {
                load: function(userAuthService){
                    return userAuthService.load()
                }
            }
        })
        .when('/myprofile', {
            templateUrl: '/partials/myprofile.html',
            resolve: {
                load: function(userAuthService){
                    return userAuthService.load()
                }
            }
        })
        .otherwise({
            redirectTo: '/auth/'
        });
}]);

app.run(function($location, userAuthService) {
    $location.search({loaded: 'true'});
    FB.init({
        appId      : '942175385799201',
        cookie     : true,  // enable cookies to allow the server to access
        xfbml      : true,  // parse social plugins on this page
        version    : 'v2.0' // use version 2.0
    });
    userAuthService.checkLogin();
});

app.factory('userAuthService', ['$q', '$http', '$rootScope', '$cookieStore', '$location', function ($q, $http, $rootScope, $cookieStore, $location){
    return {
        authLoad : function (){
            var defer = $q.defer()
            if (!$rootScope.user) defer.resolve()
            else {
                $location.path('/gamepin');
            }
        },
        load : function (){
            var defer = $q.defer()
                , socialUser = $rootScope.checkSocialAuth();

            $rootScope.remoteHost = window.remoteHost
            if (!$rootScope.user) {
                $http.get(window.location.origin).success(function(data, status, headers){
                    $http.post(window.remoteHost + '/auth/login/social-mlb', {user: JSON.stringify(socialUser)}, {withCredentials : true})
                        .success(function(data){
                            $http.get(window.remoteHost + '/auth/me', {withCredentials : true})
                                .success(function(data){
                                    console.log('data', data)
                                    $rootScope.user = data.passport.user;
                                    if(!$rootScope.user.username) $rootScope.notName = true
                                    $rootScope.user.scoreShow = Math.round($rootScope.user.score)
                                    defer.resolve();
                                })
                        }).error(function(err){
                            console.log('error', err)
                        })
                })
                return defer.promise;
            } else {
                defer.resolve();
            }

        },
        checkLogin: function(){
            $http.get(window.remoteHost + '/auth/me', {withCredentials : true})
                .success(function(data){
                    console.log('data', data)
                    if (!data.passport.user) return $location.path('/auth');

                    $rootScope.user = data.passport.user;
                    if(!$rootScope.user.username) $rootScope.notName = true
                    $rootScope.user.scoreShow = Math.round($rootScope.user.score)
                    $location.path('/gamepin')
                })
        }
    };
}]);


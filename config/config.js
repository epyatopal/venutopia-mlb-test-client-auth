module.exports = {
    development: {
        facebook: {
            clientID: '277871775731939'
            , clientSecret: 'f8056d78cde536cabbe7743b698b8842'
            , callbackURL: "http://localhost:9000/auth/facebook/callback"
        }
        , google: {
            clientID: '1035862725548-vs2nfrj2d5sairoq5qfluvsras1jtv1c.apps.googleusercontent.com'
            , clientSecret: 'kXYK7ytORjDpiLktL_NBhUKt'
            , callbackURL: "http://localhost:9000/auth/google/callback"
        }
        , remote_url: "http://localhost:3000"
        , url: "http://localhost:9000"
    },
    staging: {
        facebook: {
            clientID: '277878039064646'
            , clientSecret: '5444614f38566bc74a35f6d27f58e274'
            , callbackURL: "http://staging-mlb.venutopia.com/auth/facebook/callback"
        }
        , google: {
            clientID: '1035862725548-o3viil5fu79aoae7jlhn2epdov5gpb0f.apps.googleusercontent.com'
            , clientSecret: 'cJR8z6tss5NgJ-YeTDH1hI-s'
            , callbackURL: "http://staging-mlb.venutopia.com/auth/google/callback"
        }
        , remote_url: "http://staging.venutopia.com"
        , url: "http://staging-mlb.venutopia.com"
    },
    qa: {
        facebook: {
            clientID: ''
            , clientSecret: ''
            , callbackURL: "http://oakatpqa.bamnetworks.com/auth/facebook/callback"
        }
        , google: {
            clientID: ''
            , clientSecret: ''
            , callbackURL: "http://oakatpqa.bamnetworks.com/auth/google/callback"
        }
        , remote_url: "http://qa.venutopia.com"
        , url: "http://oakatpqa.bamnetworks.com"
    },
    production: {
        facebook: {
            clientID: ''
            , clientSecret: ''
            , callbackURL: "http://localhost:9000/auth/facebook/callback"
        }
        , google: {
            clientID: ''
            , clientSecret: ''
            , callbackURL: "http://oakatp.bamnetworks.com/auth/google/callback"
        }
        , remote_url: "http://prod.venutopia.com"
        , url: "http://oakatp.bamnetworks.com"
    }
}
var express = require('express');
var router = express.Router();
var env = process.env.NODE_ENV || 'staging'
var config = require('../config/config')[env];
/* GET home page. */
router.get('/', function(req, res) {

    var userid = req.header('X-MLBAM-WUID') || 'test-id';
    res.header('X-MLBAM-WUID', userid);

    req.app.emit('event:log_info', 'req.header(\'X-MLBAM-WUID\')=' + req.header('X-MLBAM-WUID'));
    req.app.emit('event:log_info', 'config.remote_url=' + config.remote_url + ', userid=' + userid + ', headers.host=' +req.headers.host);
    res.render('index', { config: config });
});


router.get('/test', function(req, res) {
    // var userid = req.header('X-MLBAM-WUID') || 'test-id';
    // res.header('X-MLBAM-WUID', userid);
    res.render('test');
});


module.exports = router;

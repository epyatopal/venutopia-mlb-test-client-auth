var FacebookStrategy = require('passport-facebook').Strategy
    , GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
    , TwitterStrategy = require('passport-twitter').Strategy
    , LocalStrategy = require('passport-local').Strategy
    , env = process.env.NODE_ENV || 'development'
    , config = require('../config/config')
//    , user = require('../routes/user')
//    , mongoose = require('mongoose')
//    , User = mongoose.model('User')
//    , Campaign = mongoose.model('Campaign')

exports.boot = function (app, passport, auth) {

    app.get('/', function(req, res) {
        res.render('auth/login')
    })

//    app.post('/login', function(req, res){
//        console.log('11111 user user user user user user ======>>>>>>>>> ', req.session);
//        if ((!req.body.username)) {
//            return res.send(401)
//        }
//        user.findOrCreateMLBUser(req.body.username, function(err, user){
//            if (err) return res.send(401);
//            req.session.passport.user = user;
//            console.log('3333 user user user user user user ======>>>>>>>>> ', req.session);
//            res.send(user);
//        })
//    })


    app.post('/login',
        passport.authenticate('local', { failureRedirect: '/auth_error' }),
        function(req, res) {

            app.emit('event:log_info', 'Login remote user: '+JSON.stringify(req.user)+' ('+(req.headers.origin || req.headers.host)+')');
            res.json(req.user)
            //res.redirect('/');
        });

    app.get('/facebook', function (req, res) {
        passport.authenticate('facebook')(req, res)
    });

    app.get('/facebook/callback',
        passport.authenticate('facebook', { successRedirect: '/',
                failureRedirect: '/auth'}
        ));

    app.get('/google', function (req, res) {
        passport.authenticate('google')(req, res)
    });

    app.get('/google/callback',
        passport.authenticate('google', { successRedirect: '/',
                failureRedirect: '/auth'}
        ));

    app.get('/twitter', function (req, res) {
        passport.authenticate('twitter')(req, res)
    });

    app.get('/twitter/callback',
        passport.authenticate('twitter', { successRedirect: '/',
                failureRedirect: '/auth'}
        ));

    app.get('/logout', function(req, res){
        req.logOut();
        res.clearCookie("user");
        res.send(200)
        res.redirect('/auth');

    });

    app.get('/me', function(req, res){
        res.send(req.session)

    });

    passport.use(new FacebookStrategy({
            clientID: config.facebook.clientID,
            clientSecret: config.facebook.clientSecret,
            callbackURL: config.facebook.callbackURL,
            scope: 'email' //, user_birthday'
//            passReqToCallback: true
        },
        function (accessToken, refreshToken, profile, done) {
            user.findOrCreateFacebookUser(profile, done);
        }
    ));

    passport.use(new GoogleStrategy({
            clientID: config.google.clientID,
            clientSecret: config.google.clientSecret,
            callbackURL: config.google.callbackURL,
            scope: ['profile', 'email']
        },
        function (accessToken, refreshToken, profile, done) {
            user.findOrCreateGoogleUser(profile, done);
        }
    ));

    passport.use(new TwitterStrategy({
            consumerKey: config.twitter.consumerKey,
            consumerSecret: config.twitter.consumerSecret,
            callbackURL: config.twitter.callbackURL,
            scope: 'email'
        },
        function (accessToken, refreshToken, profile, done) {
            user.findOrCreateTwitterUser(profile, done);
        }
    ));

    passport.use(new LocalStrategy(
        function(username, password, done) {
            console.log('here!!!!!---------------->>>>>>>>', username, password)

//            User.findOrCreate({ username: username }, function (err, user) {

            user.findOrCreateMLBUser(username, function(err, user){
                if (err) { return done(err); }
                if (!user) { return done(null, false); }
//                if (!user.verifyPassword(password)) { return done(null, false); }
                return done(null, user);
            });
        }
    ));

    passport.serializeUser(function (user, done) {
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        done(null, user);
    });

}